<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;

class UserService
{
    public function __construct()
    {
        $this->userRepo = new UserRepository;
    }

    /**
     * 確認憑證是否存在
     *
     * @param string $email
     * @return void
     */
    public function checkAccount($email)
    {
        return $this->userRepo->checkAccount($email);
    }

    /**
     * 授權金耀產生模組
     *
     * @return void
     */
    public function token()
    {
        $indexes = [
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z'
        ];

        //依照需求，採取後測式do...while迴圈判斷如果沒有產生會導致系統衝突的授權金鑰就跳出迴圈產生uuid合併封裝。
        do {
            $token = '';

            for ($i = 1; $i <= 12; $i++) {
                $token .= $indexes[rand(0, (count($indexes) - 1))];
            }
        } while ($this->userRepo->checkToken($token));

        return $token;
    }

    /**
     * 寄出資料郵件
     *
     * @param string $email
     * @param  array $token
     * @return void
     */
    public function mail($email, $token)
    {
        if (count($this->userRepo->checkAccount($email)) > 0) {
            $data = [];
            return Mail::send('email', $data, function ($message) use ($email, $token) {
                $message->to($email, 'user');
                $message->subject('RRR');
            });
        } else {
            $data = [];
            foreach ($token as $key => $value) {
                $data[$key] = $value;
            }
            $this->userRepo->createAccount($email,$data);
            return Mail::send('newemail', $data, function ($message) use ($email,$token) {
                $message->to($email, 'user');
                $message->subject('新申辦案件編號:' . $token['uuid']);
            });
        }
    }
}
