<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Ramsey\Uuid\Uuid;

class SettingService
{
    public function __construct()
    {
        $this->userRepo = new UserRepository;
    }

    /**
     * 確認憑證是否存在
     *
     * @param string $email
     * @return void
     */
    public function checkAccount($email)
    {
        return $this->userRepo->checkAccount($email);
    }
}
