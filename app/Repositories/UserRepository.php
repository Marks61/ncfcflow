<?php

namespace App\Repositories;

use App\Users;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function checkAccount($email)
    {
        return Users::where('name', $email)->get();
    }

    public function checkToken($token)
    {
        return (count(Users::where('token', $token)->get()) > 0);
        /*if (count(Users::where('token', $token)->get()) > 0) {
            return true;
        } else {
            return false;
        }*/
    }

    public function createAccount($email, $data)
    {
        Users::create([
            'uuid' => $data['uuid'],
            'name' => $email,
            'token' => Hash::make($data['token'])
        ]);
    }
}
