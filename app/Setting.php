<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = "setting";
    protected $primaryKey = "id";
    protected $fillable = ['uuid', 'users', 'ad', 'coaut', 'cd', 'creates', 'updates', 'is_use', 'is_lock', 'cover'];
    public $timestamps = false;
}
