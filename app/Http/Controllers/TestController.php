<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{

    /**
     * 番茄炒蛋食材 arrayCombine練習
     *
     * @return void
     */
    public function arrayCombine()
    {
        $array_key = array('000', '001', '002');

        $array_value = array('洋蔥', '番茄', '炒蛋');

        $food = array_combine($array_key, $array_value);

        dd($food);
    }

    /**
     * 公務車本地 => 遠端資料同步(1) arrayOperation練習
     *
     * @return void
     */
    public function arrayOperation()
    {
        $local = array('AJT-3364', 'AJT-0926', 'RSK-5512', 'RSK-9998', 'BER-9851', 'ADD-4714');
        $remote = array('AJT-3364', 'AJT-0926');

        if ($local == $remote) {
            echo ('本地車籍資料與遠端相符!!');
            return $this->arraySearch($remote, 1, 1);
        } else {
            echo ('遠端車籍資料完成更新!!');
            //更新車籍資料$local to $remote
            if (count($local) >= count($remote)) {
                $remote = $local + $remote;
                return $this->arraySearch($remote, 1, 1);
            } else {
                unset($remote[array_key_first(array_diff($remote, $local))]);
                return $this->arraySearch($remote, 1, 1);
            }
        }
    }

    /**
     * 公務車本地 => 遠端資料同步(2) array進階及字體大小寫轉換
     */
    public function arraySearch(...$cars)
    {
        $car = [];

        sort($cars[0]);

        //根據車牌第一位字元作為class分類，流水號規則[class+流水號]，流水號根據車牌排序 小 => 大
        foreach ($cars[0] as $array) {

            if (!isset($car[substr($array, 0, 1)])) {
                $i = 1;
            }

            $car[substr($array, 0, 1)][substr($array, 0, 1) . sprintf("%'03d", $i)] = $array;
            $i++;
        }

        //轉小寫
        //dump(strtolower($car['A']['A001']));

        //轉大寫
        //dump(strtoupper($car['A']['A001']));

        //echo substr($car['A']['A001'], 0, 3);

        return $car;
    }

    public function test1()
    {
        $array = [];

        $array[0] = [
            'firstValue' => 1,
            'firstRelation' => '<',
            'secondValue' => 2,
            'secondRelation' => '<='
            ];
            
        $array[2] = [
            'firstValue' => 0,
            'firstRelation' => '<',
            'secondValue' => '',
            'secondRelation' => ''

        ];

        $value = [];

        $array[0] = [
            'firstValue' => 1,
            'firstRelation' => '<',
            'secondValue' => 5,
            'secondRelation' => '<='
        ];

        $strings = '';
        
        $array[1] = [
            'firstValue' => 5,
            'firstRelation' => '<',
            'secondValue' => 7,
            'secondRelation' => '<='
        ];


        foreach ($array as $data) {
            array_push($value, $data['firstValue']);
        }

        sort($value);

        unset($value[0]);

        foreach ($value as $ans) {
            foreach ($array as $relation) {
                switch ($relation['firstRelation']) {
                    case '<':
                        if (isset($relation['secondRelation'])) {
                            switch ($relation['secondRelation']) {
                                case '<':
                                    if ($relation['firstValue'] < $ans && $ans < $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                                case '>':
                                    if ($relation['firstValue'] < $ans && $ans > $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                                case '<=':
                                    if ($relation['firstValue'] < $ans && $ans <= $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                            }
                        } else {
                            if ($relation['firstValue'] < $ans) {
                                echo '正確';
                                break 3;
                            }
                        }
                        break;
                    case '>':
                        if (isset($relation['secondRelation'])) {
                            switch ($relation['secondRelation']) {
                                case '<':
                                    if ($relation['firstValue'] > $ans && $ans < $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                                case '>':
                                    if ($relation['firstValue'] > $ans && $ans > $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                                case '<=':
                                    if ($relation['firstValue'] > $ans && $ans <= $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                            }
                        } else {
                            if ($relation['firstValue'] > $ans) {
                                echo '正確';
                            } else {
                                echo '錯誤';
                            }
                        }
                        break;
                    case '<=':
                        if (isset($relation['secondRelation'])) {
                            switch ($relation['secondRelation']) {
                                case '<':
                                    if ($relation['firstValue'] <= $ans && $ans < $relation['secondValue']) {

        array_multisort($array, SORT_ASC, $value);
        foreach ($array as $relation) {
            $value1 = [];
            if (!empty($value1)) {
                unset($value1[0]);
                $value2 = [];
                array_push($value1, $relation['firstValue']);
                if (!$relation['secondValue'] === '') {
                    array_push($value1, $relation['secondValue']);
                }
                if ($value1[1] === $value2[0]) {
                    foreach ($value2 as $r) {
                        if ($r == 0) {
                            break;
                        } else {
                            switch ($relation['firstRelation']) {
                                case '<':
                                    if (isset($relation['secondRelation'])) {
                                        switch ($relation['secondRelation']) {
                                            case '<':
                                                if ($relation['firstValue'] < $r && $r < $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                            case '>':
                                                if ($relation['firstValue'] < $r && $r > $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                            case '<=':
                                                if ($relation['firstValue'] < $r && $r <= $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                        }
                                    } else {
                                        if ($relation['firstValue'] < $r) {
                                            echo '正確';
                                        }
                                    }
                                    break;
                                case '>':
                                    if (isset($relation['secondRelation'])) {
                                        switch ($relation['secondRelation']) {
                                            case '<':
                                                if ($relation['firstValue'] > $r && $r < $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                            case '>':
                                                if ($relation['firstValue'] > $r && $r > $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                            case '<=':
                                                if ($relation['firstValue'] > $r && $r <= $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                        }
                                    } else {
                                        if ($relation['firstValue'] > $r) {
                                            echo '正確';
                                        } else {
                                            echo '錯誤';
                                        }
                                    }
                                    break;
                                case '<=':
                                    if (isset($relation['secondRelation'])) {
                                        switch ($relation['secondRelation']) {
                                            case '<':
                                                if ($relation['firstValue'] <= $r && $r < $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                            case '>':
                                                if ($relation['firstValue'] <= $r && $r > $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                            case '<=':
                                                if ($relation['firstValue'] <= $r && $r <= $relation['secondValue']) {
                                                    echo '正確';
                                                } else {
                                                    echo '錯誤';
                                                }
                                                break;
                                        }
                                    } else {
                                        if ($relation['firstValue'] <= $r) {
                                            echo '正確';
                                            break 3;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    $value1[1] = $value2[1];
                } else {
                    $this->error = '錯誤';
                    return false;
                    break;
                }
            } else {
                array_push($value1, $relation['firstValue']);
                if (!$relation['secondValue'] === '') {
                    array_push($value1, $relation['secondValue']);
                }
                foreach ($value1 as $r) {
                    if ($r == 0) {
                        break;
                    } else {
                        switch ($relation['firstRelation']) {
                            case '<':
                                if (isset($relation['secondRelation'])) {
                                    switch ($relation['secondRelation']) {
                                        case '<':
                                            if ($relation['firstValue'] < $r && $r < $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                        case '>':
                                            if ($relation['firstValue'] < $r && $r > $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                        case '<=':
                                            if ($relation['firstValue'] < $r && $r <= $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                    }
                                } else {
                                    if ($relation['firstValue'] < $r) {
                                        echo '正確';
                                    }
                                }
                                break;
                            case '>':
                                if (isset($relation['secondRelation'])) {
                                    switch ($relation['secondRelation']) {
                                        case '<':
                                            if ($relation['firstValue'] > $r && $r < $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                        case '>':
                                            if ($relation['firstValue'] > $r && $r > $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                        case '<=':
                                            if ($relation['firstValue'] > $r && $r <= $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                    }
                                } else {
                                    if ($relation['firstValue'] > $r) {

                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }

                                    break;
                                case '>':
                                    if ($relation['firstValue'] <= $ans && $ans > $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                                case '<=':
                                    if ($relation['firstValue'] <= $ans && $ans <= $relation['secondValue']) {
                                        echo '正確';
                                    } else {
                                        echo '錯誤';
                                    }
                                    break;
                            }
                        } else {
                            if ($relation['firstValue'] <= $ans) {
                                echo '正確';
                                break 3;
                            }
                        }
                        break;

                                }
                                break;
                            case '<=':
                                if (isset($relation['secondRelation'])) {
                                    switch ($relation['secondRelation']) {
                                        case '<':
                                            if ($relation['firstValue'] <= $r && $r < $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                        case '>':
                                            if ($relation['firstValue'] <= $r && $r > $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                        case '<=':
                                            if ($relation['firstValue'] <= $r && $r <= $relation['secondValue']) {
                                                echo '正確';
                                            } else {
                                                echo '錯誤';
                                            }
                                            break;
                                    }
                                } else {
                                    if ($relation['firstValue'] <= $r) {
                                        echo '正確';
                                        break 3;
                                    }
                                }
                                break;
                        }
                    }

                }
            }
        }
    }


    public function test2()
    {
        $merage = [];
        $array[0] = ['sort' => 0, 'value' => 10];
        $array[1] = ['sort' => 1, 'value' => 20];
        array_push($merage,$array[0]);
        array_push($merage,$array[1]);
        dump($merage);
    }

}
