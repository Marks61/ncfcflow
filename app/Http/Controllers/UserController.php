<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailRequest;
use App\Services\SettingService;
use App\Services\UserService;
use App\Users;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{
    public function __construct()
    {
        $this->userService = new UserService;
        $this->settingUser = new SettingService;
    }

    /**
     * 主程式
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        /*if條件:檔案上傳例外處理--授權碼為空，先判斷信箱資料存不存在
          else條件:上傳檔案*/
        if ($request->token == '') {
            $check = $this->userService->checkAccount($request->email);
            if (count($check) > 0) {
                return response()->json([
                    'status' => 'success',
                    'code' => '03001002',
                    'data' => '確認重設授權?'
                ], JsonResponse::HTTP_OK);
            } else {
                $name = $this->userService->token();
                $this->userService->mail($request->email, array('token' => $name, 'uuid' => Uuid::uuid4()));
                return response()->json([
                    'status' => 'success',
                    'code' => '03001001',
                    'data' => '授權碼已經建立!!請至信箱確認。'
                ], JsonResponse::HTTP_OK);
            }
        } else {
            //$this->fileService->uploadfile($request);
        }
    }

    public function setting(Request $request)
    {
        $check = $this->settingUser->checkAccount($request->email);
        if (count($check) > 0) {
            return response()->json([
                'status' => 'success',
                'code' => '03002001',
                'data' => 'uuid:' . $check[0]['uuid']
            ], JsonResponse::HTTP_OK);
        } else {
            return response()->json([
                'status' => 'fail',
                'code' => '03002404',
                'data' => '搜尋不到操作資料。'
            ], JsonResponse::HTTP_NOT_FOUND);
        }
    }

    public function signup(EmailRequest $request)
    {
        $name = $this->userService->token();
        $this->userService->mail($request->mail, array('token' => $name, 'uuid' => Uuid::uuid4()));
        return '<script>alert("帳戶建立成功!!請至信箱確認。");location.href = "'.url('/signin').'";</script>';
    }
}
