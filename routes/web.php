<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/arrayCombine', 'TestController@arrayCombine')->name('arrayCombine');
Route::any('/arrayOperation', 'TestController@arrayOperation')->name('arrayOperations');
Route::any('/test1', 'TestController@test1')->name('test1');
Route::any('/test2', 'TestController@test2')->name('test2');
Route::any('/signup', function () {
    return view('sign.signup');
});
Route::post('/signup/create', 'UserController@signup');
