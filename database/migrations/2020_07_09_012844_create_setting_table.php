<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->comment('設定檔Id');
            $table->string('users')->comment('使用者uuid');
            $table->integer('ad')->comment('權限:0最高;1可以新增編輯，不可複核;2一般用戶');
            $table->integer('coaut')->comment('可用空間');
            $table->integer('cd')->comment('上傳數量上限');
            $table->string('creates')->comment('建立');
            $table->string('updates')->comment('更新');
            $table->boolean('is_use')->comment('狀態');
            $table->boolean('is_lock')->comment('鎖定');
            $table->string('cover')->comment('複核');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
